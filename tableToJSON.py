import requests
from bs4 import BeautifulSoup
import sys



class MapResult(object):
    def __init__(self, map, points):
        self.map = map
        self.points = points

class Player(object):
    def __init__(self, name, result, mapResults):
        self.name = name
        self.result = result
        self.mapResults = mapResults
    def toString(self):
        return self.name + ': ' + str(self.result)
    def getJSON(self, tab_shift):
        shift = ""
        for i in range(tab_shift):
            shift += "\t" 

        string =  shift + "{\n" + shift + "\t\"name\": " + "\"" + self.name + "\"" + ',\n' + \
            shift + "\t\"result\": " +  str(self.result) + ',\n' + \
            shift + "\t\"mapResults\": [\n"
        for i in range(len(self.mapResults) - 1):
            string += shift + "\t\t{ \"map\": " + "\"" + self.mapResults[i].map + "\"" + ", \"points\": " + \
                str(self.mapResults[i].points) + "},\n"
        string += shift + "\t\t{ \"map\": " + "\"" + self.mapResults[len(self.mapResults)-1].map + "\"" + ", \"points\": " + \
            str(self.mapResults[len(self.mapResults)-1].points) + "}\n" + shift + "\t]\n" + shift +"}"
        return string

def get_content(url):
    r = requests.get(url)
    r.encoding = 'utf-8'
    return r.text

def get_players(url):
    soup = BeautifulSoup(get_content(url), 'html.parser')
    # headers of columns
    th_contents = []

    thead = soup.find('table').find('thead')
    tbody = soup.find('table').find('tbody')
    
    for th in thead.find_all('th'):
        if th.has_attr('class'): 
            th_contents.append(th['class'][0]) 
        else:
            a = th.find('a')
            if a:
                img = a.find('img')
                if img:
                    if img.has_attr('src'):
                        th_contents.append(img['src'].split('/')[-1].split('.')[0])      
                    else:
                        th_contents.append('undefined name')
                elif a.has_attr('href'):
                    th_contents.append(img['href'])
                else:
                    th_contents.append('undefined name')
            else:
                th_contents.append('undefined name')    
    players = []
    for tr in tbody.find_all('tr'):
        mapResults = []
        #tr_contents.append(tr.text.strip())
        #tr_contents.append(tr)
        for i in range(8, len(th_contents)):
            if tr.select('td')[i].text == '':
                mapResults.append(MapResult(map = th_contents[i], points = 0))
            else:
                mapResults.append(MapResult(map = th_contents[i], points = int(tr.select('td')[i].text)))
        players.append(Player(name=tr.select('td')[1].text, result = int(tr.select('td')[7].text), mapResults = mapResults))
        #print(tr)
        
    return players
    #print(tr_contents)

if __name__ == "__main__":
    mode = ""
    url = ""
    output = ""
    if len(sys.argv) < 4:
        print('Set a mode')
        mode = input()
        print('Set an url to page with table')
        url = input()
        print('Set a name of output file')
        output = open(input() + '.json', 'w')
    else:
        mode = sys.argv[1]
        url = sys.argv[2]
        output = open(sys.argv[3] + '.json', 'w')
    players = get_players(url)
    
    output.write("{\n")
    output.write("\t\"mode\": " + "\"" + mode + "\",\n")
    output.write("\t\"records\": [\n")
    for i in range(len(players)-1):
        output.write(players[i].getJSON(2)+ ",\n")
    output.write(players[len(players)-1].getJSON(2) + '\n')
    output.write("\t]\n")
    output.write("}\n")