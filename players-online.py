import requests
from bs4 import BeautifulSoup
import sys

def get_content(url = "https://online.supertuxkart.net/api/v2/server/get-all"):
    r = requests.get(url)
    r.encoding = 'utf-8'
    return r.text

def get_players(url = "https://online.supertuxkart.net/api/v2/server/get-all"):
    soup = BeautifulSoup(get_content(url), 'html.parser')
    # headers of columns
    th_contents = []

    servers = soup.find('servers')
    for server in servers.find_all('server'):
        serv_info = server.find('server-info')
        if int(serv_info['current_players']) > 0:
            players = server.find('players')
            if len(players) > 0:
                print('------------------------------------------------------')
                print("Server:", serv_info['name'])
                print('Current players: ', len(players))
                for playerInfo in players.find_all('player-info'):
                    if playerInfo.has_attr('scores'):
                        print('\tName:', playerInfo['username'], ', score:', playerInfo['scores'])
                    else:
                        print('\tName:', playerInfo['username'])



if __name__ == "__main__":
    get_players()